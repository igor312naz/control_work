$(document).ready(function() {
    $("#reviewsSlider").on("slide.bs.carousel", function(e) {
        var $e = $(e.relatedTarget);
        var idx = $e.index();
        var itemsPerSlide = 3;
        var totalItems = $("#reviewsSlider .carousel-item").length;

        if (idx >= totalItems - (itemsPerSlide - 1)) {
            var it = itemsPerSlide - (totalItems - idx);
            for (var i = 0; i < it; i++) {
                // append slides to end
                if (e.direction == "left") {
                    $("#reviewsSlider .carousel-item")
                        .eq(i)
                        .appendTo("#reviewsSlider .carousel-inner");
                } else {
                    $("#reviewsSlider .carousel-item")
                        .eq(0)
                        .appendTo($(this).find("#reviewsSlider .carousel-inner"));
                }
            }
        }
    });
});
