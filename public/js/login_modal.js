$(function () {
    $('#app_login').on('click', function () {
        $('#login-modal').modal();

        return false;
    });

    $('#json-login-send').on('click', function () {
        $.ajax(
            {
                url: "/jlogin",
                type: "POST",
                data: JSON.stringify({
                    username: $('#json-username').val(),
                    password: $('#json-password').val()
                }),
                headers: {
                    'Content-Type': 'application/json'
                },
                dataType: "json"
            }).done(function () {
            document.location = '/';
        }).fail(function (data, textStatus, xhr) {
            var error_text = 'При авторизации возникла ошибка.';

            if(data.responseJSON.error = 'Invalid credentials.'){
                error_text = 'Введены неверные данные!';
            }

            $('.login-error').html(error_text);
            $('#login-modal').modal();
        });
    });
});