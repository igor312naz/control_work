$(document).ready(function() {
    $("#videoSlider").on("slide.bs.carousel", function(e) {
        var $e = $(e.relatedTarget);
        var idx = $e.index();
        var itemsPerSlide = 3;
        var totalItems = $("#videoSlider .carousel-item").length;

        if (idx >= totalItems - (itemsPerSlide - 1)) {
            var it = itemsPerSlide - (totalItems - idx);
            for (var i = 0; i < it; i++) {
                // append slides to end
                if (e.direction == "left") {
                    $("#videoSlider .carousel-item")
                        .eq(i)
                        .appendTo("#videoSlider .carousel-inner");
                } else {
                    $("#videoSlider .carousel-item")
                        .eq(0)
                        .appendTo($(this).find("#videoSlider .carousel-inner"));
                }
            }
        }
    });
});
