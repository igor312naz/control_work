<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ImageRepository")
 * @Vich\Uploadable
 */
class Image
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $image;

    /**
     * @Vich\UploadableField(mapping="images", fileNameProperty="image")
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\GalleryImage", inversedBy="image", cascade={"persist", "remove"})
     */
    private $galleryImage;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return null|string
     */
    public function getImage(): ?string
    {
        return $this->image;
    }

    /**
     * @param string $image
     * @return Image
     */
    public function setImage(string $image = null): ?self
    {
        $this->image = $image;

        return $this;
    }

    /**
     * @param File $imageFile
     *
     * @return Image
     */
    public function setImageFile(File $imageFile)
    {
        $this->imageFile = $imageFile;
        return $this;
    }

    /**
     * @return File
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * @return GalleryImage|null
     */
    public function getGalleryImage(): ?GalleryImage
    {
        return $this->galleryImage;
    }

    /**
     * @param GalleryImage|null $galleryImage
     * @return Image
     */
    public function setGalleryImage(?GalleryImage $galleryImage): self
    {
        $this->galleryImage = $galleryImage;

        return $this;
    }
}
