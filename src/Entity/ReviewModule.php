<?php


namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ReviewRepository")
 */
class ReviewModule
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string" , length=1024)
     */
    private $name;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    private $isActive;

    /**
     * @var string
     *
     * @ORM\Column(type="string" , length=1024)
     */
    private $link;

       /**
     * @return string
     */
    public function getLink(): ?string
    {
        return $this->link;
    }

    /**
     * @param string $link
     * @return ReviewModule
     */
    public function setLink(string $link): ReviewModule
    {
        $this->link = $link;
        return $this;
    }

    /**
     * @param string $name
     * @return ReviewModule
     */
    public function setName(string $name): ReviewModule
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param bool $isActive
     * @return ReviewModule
     */
    public function setIsActive(bool $isActive): ReviewModule
    {
        $this->isActive = $isActive;
        return $this;
    }

    /**
     * @return bool
     */
    public function isActive(): ?bool
    {
        return $this->isActive;
    }


}