<?php


namespace App\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EventSessionRepository")
 */
class EventSession
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;


    /**
     * @var string
     *
     * @ORM\Column(type="string" , length=1024)
     */
    private $name;

    /**
     * @var EventModule
     * @ORM\ManyToOne(targetEntity="App\Entity\EventModule", inversedBy="eventSessions")
     */
    private $event;

    /**
     * @Assert\Expression(
     *     "this.getDate() >= this.getEvent().getEventStart()",
     *      message="Дата сеанса должна больше даты начала мероприятия"
     * )
     * @Assert\Expression(
     *     "this.getDate() <= this.getEvent().getEventEnding()",
     *      message="Дата сеанса должна меньше даты окончания мероприятия"
     * )
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date;

    /**
     * @var string
     * @ORM\Column(type="decimal")
     */
    private $price;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="EventFile", mappedBy="event", cascade={"persist", "remove"})
     */
    private $eventFiles;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="EventBooking", mappedBy="event", cascade={"persist", "remove"})
     */
    private $eventBookings;

    public function __construct()
    {
        $this->eventFiles = new ArrayCollection();
        $this->eventBookings = new ArrayCollection();
        $this->setDate(new DateTime());
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function setPrice($price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getEvent(): ?EventModule
    {
        return $this->event;
    }

    public function setEvent(?EventModule $event): self
    {
        $this->event = $event;

        return $this;
    }

    /**
     * @return Collection|EventFile[]
     */
    public function getEventFiles(): Collection
    {
        return $this->eventFiles;
    }

    public function addEventFile(EventFile $eventFile): self
    {
        if (!$this->eventFiles->contains($eventFile)) {
            $this->eventFiles[] = $eventFile;
            $eventFile->setEvent($this);
        }

        return $this;
    }

    public function removeEventFile(EventFile $eventFile): self
    {
        if ($this->eventFiles->contains($eventFile)) {
            $this->eventFiles->removeElement($eventFile);
            // set the owning side to null (unless already changed)
            if ($eventFile->getEvent() === $this) {
                $eventFile->setEvent(null);
            }
        }

        return $this;
    }

    /**
     * @return ArrayCollection|EventBooking[]
     */
    public function getEventBookings(): PersistentCollection
    {
        return $this->eventBookings;
    }

    public function addEventBooking(EventBooking $eventBooking): self
    {
        if (!$this->eventBookings->contains($eventBooking)) {
            $this->eventBookings[] = $eventBooking;
            $eventBooking->setEvent($this);
        }

        return $this;
    }

    public function removeEventBooking(EventBooking $eventBooking): self
    {
        if ($this->eventBookings->contains($eventBooking)) {
            $this->eventBookings->removeElement($eventBooking);
            // set the owning side to null (unless already changed)
            if ($eventBooking->getEvent() === $this) {
                $eventBooking->setEvent(null);
            }
        }

        return $this;
    }

    /**
     * @param \DateTime $date
     * @return EventSession
     */
    public function setDate(\DateTime $date): EventSession
    {
        $this->date = $date;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDate(): \DateTime
    {
        return $this->date;
    }

    public function __toString()
    {
        return "Сеанс" . " " . $this->getId();
    }

    /**
     * @param string $name
     * @return EventSession
     */
    public function setName(string $name): EventSession
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getName():? string
    {
        return $this->name;
    }
}