<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Vich\UploaderBundle\Mapping\Annotation as Vich;


/**
 * @ORM\Entity(repositoryClass="App\Repository\GalleryImageRepository")
 *
 */
class GalleryImage
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\GalleryModule", inversedBy="images")
     * @ORM\JoinColumn(nullable=false)
     */
    private $gallery;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Image", mappedBy="galleryImage", cascade={"persist", "remove"})
     */
    private $image;

    /**

     * @var integer $position

     *@Gedmo\Sortable()

     * @ORM\Column(name="position", type="integer")

     */

    private $position;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return GalleryModule|null
     */
    public function getGallery(): ?GalleryModule
    {
        return $this->gallery;
    }

    /**
     * @param GalleryModule|null $gallery
     * @return GalleryImage
     */
    public function setGallery(?GalleryModule $gallery): self
    {
        $this->gallery = $gallery;

        return $this;
    }

    /**
     * @return Image|null
     */
    public function getImage(): ?Image
    {
        return $this->image;
    }

    /**
     * @param Image|null $image
     * @return GalleryImage
     */
    public function setImage(?Image $image): self
    {
        $this->image = $image;

        // set (or unset) the owning side of the relation if necessary
        $newGalleryImage = $image === null ? null : $this;
        if ($newGalleryImage !== $image->getGalleryImage()) {
            $image->setGalleryImage($newGalleryImage);
        }

        return $this;
    }

    /**

     * @return int

     */

    public function getPosition()

    {

        return $this->position;

    }

    /**

     * @param int $position

     */

    public function setPosition($position)

    {

        $this->position = $position;

    }

}
