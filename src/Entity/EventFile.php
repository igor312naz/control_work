<?php


namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EventFileRepository")
 * @Vich\Uploadable
 */
class EventFile
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string" , length=1024)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(type="string" , length=1024)
     */
    private $material;

    /**
     * @Vich\UploadableField(mapping="event_material", fileNameProperty="material")
     * @var File
     */
    private $fileMaterial;

    /**
     * @var EventSession
     * @ORM\ManyToOne(targetEntity="App\Entity\EventSession", inversedBy="eventFiles")
     */
    private $event;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $name
     * @return EventFile
     */
    public function setName(string $name): EventFile
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $material
     * @return EventFile
     */
    public function setMaterial(?string $material): EventFile
    {
        $this->material = $material;
        return $this;
    }

    /**
     * @return string
     */
    public function getMaterial(): string
    {
        return $this->material;
    }

    /**
     * @param File $fileMaterial
     * @return EventFile
     */
    public function setFileMaterial(File $fileMaterial): EventFile
    {
        $this->fileMaterial = $fileMaterial;
        return $this;
    }

    /**
     * @return File
     */
    public function getFileMaterial()
    {
        return $this->fileMaterial;
    }

    /**
     * @param EventSession $event
     * @return EventFile
     */
    public function setEvent(EventSession $event): EventFile
    {
        $this->event = $event;
        return $this;
    }

    /**
     * @return EventSession
     */
    public function getEvent(): ?EventSession
    {
        return $this->event;
    }

    public function __toString()
    {
        return $this->name ?: "";
    }
}