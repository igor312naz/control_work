<?php


// src/Entity/User.php

namespace App\Entity;



use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\Table(name="fos_user")
 * @Vich\Uploadable
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    private $newPass;

    /**
     * @var string
     *
     * @ORM\Column(type="string" , length=1024)
     */
    private $fullName;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=1024)
     */
    private $clientType;

    /**
     *
     * @var string
     *
     * @ORM\Column(type="string",length=1024 , nullable=true)
     */
    private $organization_name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $avatar;

    /**
     * @Vich\UploadableField(mapping="avatars", fileNameProperty="avatar")
     * @var File
     */
    private $avatarFile;

    /**
     * @var string
     *
     * @ORM\Column(type="string" , length=1024)
     * @Assert\Regex(
     *     pattern="/^\+?[\s\-\(\)0-9]{7,19}$/",
     *     message="Это не номер телефона.")
     */
    private $phoneNumber;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="EventBooking", mappedBy="user", cascade={"persist", "remove"})
     */
    private $eventBookings;


    public function __construct()
    {
        parent::__construct();
        $this->eventBookings = new ArrayCollection();
    }

    /**
     * Sets the email.
     *
     * @param string $email
     * @return User
     */
    public function setEmail($email): User
    {
        $this->setUsername($email);

        return parent::setEmail($email);
    }

    /**
     * Set the canonical email.
     *
     * @param string $emailCanonical
     * @return User
     */
    public function setEmailCanonical($emailCanonical)
    {
        $this->setUsernameCanonical($emailCanonical);

        return parent::setEmailCanonical($emailCanonical);
    }

    /**
     * @param string $fullName
     * @return User
     */
    public function setFullName(string $fullName): User
    {
        $this->fullName = $fullName;
        return $this;
    }

    /**
     * @return string
     */
    public function getFullName(): ?string
    {
        return $this->fullName;
    }

    /**
     * @param string $clientType
     * @return User
     */
    public function setClientType(string $clientType): User
    {
        $this->clientType = $clientType;
        return $this;
    }

    /**
     * @return string
     */
    public function getClientType()
    {
        return $this->clientType;
    }

    /**
     * @param string $organization_name
     * @return User
     */
    public function setOrganizationName(string $organization_name): User
    {
        $this->organization_name = $organization_name;
        return $this;
    }

    /**
     * @return string
     */
    public function getOrganizationName(): ?string
    {
        return $this->organization_name;
    }

    /**
     * @return string
     */
    public function getNewPass(): ?string
    {
        return $this->newPass;
    }

    /**
     * @param mixed $newPass
     * @return User
     */
    public function setNewPass($newPass): User
    {
        $this->newPass = $newPass;
        return $this;
    }

    /**
     * @return ArrayCollection|EventBooking[]
     */
    public function getEventBookings()
    {
        return $this->eventBookings;
    }

    public function addEventBooking(EventBooking $eventBooking): self
    {
        if (!$this->eventBookings->contains($eventBooking)) {
            $this->eventBookings[] = $eventBooking;
            $eventBooking->setUser($this);
        }

        return $this;
    }

    public function removeEventBooking(EventBooking $eventBooking): self
    {
        if ($this->eventBookings->contains($eventBooking)) {
            $this->eventBookings->removeElement($eventBooking);
            // set the owning side to null (unless already changed)
            if ($eventBooking->getUser() === $this) {
                $eventBooking->setUser(null);
            }
        }

        return $this;
    }


    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;
        return $this;
    }


    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * @param null|File $avatarFile
     * @return User
     */
    public function setAvatarFile(?File $avatarFile): User
    {


        $this->avatarFile = $avatarFile;
        $this->setAvatar($avatarFile->getFilename());

        return $this;
    }

    /**
     * @return File
     */
    public function getAvatarFile()
    {
        return $this->avatarFile;
    }

    public function __toString()
    {
        return $this->fullName ?: '';
    }

    /**
     * @param string $phoneNumber
     * @return User
     */
    public function setPhoneNumber(string $phoneNumber): User
    {
        $this->phoneNumber = $phoneNumber;
        return $this;
    }

    /**
     * @return string
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

}