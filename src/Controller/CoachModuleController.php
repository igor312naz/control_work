<?php

namespace App\Controller;

use App\Repository\CoachModuleRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class CoachModuleController extends Controller
{
    /**
     * @Route("/coach/slider", name="app_coach_slider")
     * @param CoachModuleRepository $moduleRepository
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showSliderAction(CoachModuleRepository $moduleRepository)
    {
        $allCoach = $moduleRepository->findAllAndSortByPriority();
        return $this->render('coach_module/slider_coach.html.twig', [
            'coaches' => $allCoach
        ]);
    }

    /**
     * @Route("/coach/{id}", requirements={"id": "\d+"}, name="app_coach_single")
     * @param CoachModuleRepository $moduleRepository
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showSingleCouchAction(CoachModuleRepository $moduleRepository, $id)
    {
        $coach = $moduleRepository->find($id);

        return $this->render('coach_module/single_coach.html.twig', [
            'coach' => $coach
        ]);
    }

    /**
     * @Route("/coach", name="app_coach_all")
     * @param CoachModuleRepository $moduleRepository
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAllCouchAction(CoachModuleRepository $moduleRepository)
    {
        $coach = $moduleRepository->findAll();

        return $this->render('coach_module/page_coach.html.twig', [
            'coach' => $coach
        ]);
    }
}
