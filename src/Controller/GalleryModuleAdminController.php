<?php

namespace App\Controller;


use App\Entity\GalleryImage;
use App\Entity\Image;
use App\Model\Priority\PriorityHandler;
use App\Repository\GalleryImageRepository;
use App\Repository\GalleryModuleRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Sonata\AdminBundle\Controller\CRUDController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


class GalleryModuleAdminController extends CRUDController
{

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function uploadAction() {
        $object = $this->admin->getSubject();
        return $this->renderWithExtraParams('SonataAdmin/CRUD/gallery/upload.html.twig',[
        'object' => $object
            ]);

    }

    public function fileUploadHandlerAction(Request $request, $id, GalleryImageRepository $imageRepository) {
        $object = $this->admin->getSubject();
        // get the file from the request object
        $file = $request->files->get('file');
        // generate a new filename
        $fileName = md5(uniqid()).'.'.$file->guessExtension();
        // set your uploads directory
        $uploadDir = $this->getParameter('kernel.project_dir') . '/public/uploads/images/';
        if (!file_exists($uploadDir) && !is_dir($uploadDir)) {
            mkdir($uploadDir, 0775, true);
        }
        if ($file->move($uploadDir, $fileName)) {
            // get entity manager
            $em = $this->getDoctrine()->getManager();
            // create and set this ImageEntity
            $image = new Image();
            $image->setImage($fileName);
            $galleryImage = new GalleryImage();
            $galleryImage->setImage($image);
            $imageWithMaxPosition = $imageRepository->findWithMaxPosition($object->getId());
            if($imageWithMaxPosition){
                $position = $imageWithMaxPosition->getPosition() + 1;
            }else{
                $position = 0;
            }
            $galleryImage->setPosition($position);
            $galleryImage->setGallery($object);
            // save the uploaded filename to database
            $em->persist($image);
            $em->persist($galleryImage);
            $em->flush();
        };

        return new JsonResponse(true);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function sortAction($id) {
        $object = $this->admin->getSubject();

        $galleryImages = $object->getImages();

        return $this->renderWithExtraParams('SonataAdmin/CRUD/gallery/sort.html.twig',[
            'images' => $galleryImages,
            'object' => $object
        ]);

    }


    /**
     *
     * Resorts an item using it's doctrine sortable property
     * @param Request $request
     * @param GalleryImageRepository $imageRepository
     * @param ObjectManager $manager
     * @return \Symfony\Component\HttpFoundation\Response
     */

    public function sortingAction(
        Request $request,
        GalleryImageRepository $imageRepository,
        ObjectManager $manager)

    {
        $data = $request->get('sort');
        $image = $imageRepository->find($data['id']);
        $image->setPosition($data['position']);
        $manager->flush();
        return new JsonResponse(true);

    }

    /**
     * @param $id
     * @param GalleryModuleRepository $moduleRepository
     * @param ObjectManager $manager
     * @param PriorityHandler $priorityHandler
     * @return RedirectResponse
     */
    public function priorityUpAction(
        $id,
        GalleryModuleRepository $moduleRepository,
        PriorityHandler $priorityHandler,
        ObjectManager $manager)
    {
        $object = $this->admin->getSubject();

        if (!$object) {
            throw new NotFoundHttpException(sprintf('unable to find the object with id: %s', $id));
        }
        $priorityHandler->changePriorityAction($moduleRepository, $manager, $object,  -1);;

        return new RedirectResponse($this->admin->generateUrl('list'));

    }

    /**
     * @param $id
     * @param GalleryModuleRepository $moduleRepository
     * @param ObjectManager $manager
     * @param PriorityHandler $priorityHandler
     * @return RedirectResponse
     */
    public function priorityDownAction(
        $id,
        GalleryModuleRepository $moduleRepository,
        PriorityHandler $priorityHandler,
        ObjectManager $manager)
    {
        $object = $this->admin->getSubject();

        if (!$object) {
            throw new NotFoundHttpException(sprintf('unable to find the object with id: %s', $id));
        }
        $priorityHandler->changePriorityAction($moduleRepository, $manager, $object,  1);

        return new RedirectResponse($this->admin->generateUrl('list'));

    }

    /**
     * @param $id
     * @param Request $request
     * @param GalleryImageRepository $imageRepository
     * @param ObjectManager $manager
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function removeAction(
        $id,
        Request $request,
        GalleryImageRepository $imageRepository,
        ObjectManager $manager
    ) {
        $image = $imageRepository->find($id);
        $manager->remove($image);
        $manager->flush();
        return $this->redirect($request->server->get('HTTP_REFERER'));

    }

}
