<?php


namespace App\Controller;


use App\Entity\EventBooking;
use App\Entity\EventCategory;
use App\Entity\EventModule;
use App\Entity\EventSession;
use App\Form\ProgramType;
use App\Repository\EventBookingRepository;
use App\Repository\EventCategoryRepository;
use App\Repository\EventModuleRepository;
use App\Repository\SiteSettingsRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class EventsModuleController extends Controller
{

    /**
     * @Route("/events/slider", name="app_events_slider")
     * @param EventModuleRepository $eventModuleRepository
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showEventsSliderAction(EventModuleRepository $eventModuleRepository)
    {
        $events = $eventModuleRepository->getActiveEvents();
        return $this->render('events_module/slider_events.html.twig', [
            "events" => $events
        ]);
    }

    /**
     * @Route("/events", name="app_events_page")
     * @param EventModuleRepository $eventModuleRepository
     * @return \Symfony\Component\HttpFoundation\Response
     * @internal param ReviewRepository $reviewRepository
     */
    public function showAllEventsAction(EventModuleRepository $eventModuleRepository)
    {
        $events = $eventModuleRepository->getActiveEvents();

        return $this->render('events_module/page_events.html.twig', [
            'events' => $events
        ]);
    }


    /**
     * @Route("/events/category/{id}", requirements={"id": "\d+"}, name="app_events_category")
     * @param EventModuleRepository $eventModuleRepository
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAllEventsByCategoryAction(EventModuleRepository $eventModuleRepository, EventCategoryRepository $eventCategoryRepository, int $id)
    {
        /**
         * @var EventModule
         */
        $events = $eventModuleRepository->getEventsByCategory($id);
        $category = $eventCategoryRepository->find($id);

        return $this->render('events_module/category_events.html.twig', [
            'events' => $events,
            'category' => $category->getName()
        ]);
    }


    /**
     * @Route("/events/{id}", requirements={"id": "\d+"}, name="app_events_single")
     * @param EventModuleRepository $eventModuleRepository
     * @param int $id
     * @param SiteSettingsRepository $settingsRepository
     * @param \Swift_Mailer $mailer
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showSingleReviewAction(
        EventModuleRepository $eventModuleRepository,
        int $id,
        SiteSettingsRepository $settingsRepository,
        \Swift_Mailer $mailer,
        Request $request)
    {
        /**
         * @var EventModule
         */
        $event = $eventModuleRepository->getEventById($id);
        $eventSessions = $event->getEventSessions();
        $form = $this->createForm(ProgramType::class, null);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){
            $settings = $settingsRepository->getEmails();
            $mails = explode(',',$settings->getAdminEmail());
            $data = $form->getData();
            $token = "636825005:AAFhEUtGj1NOT3eIPTdwF9h9mhRLYTmiMRE";
            $chat_id = "-306125915";
            $txt = '';
            $arr = [
                'Имя пользователя: ' => $data['fullName'],
                'Телефон: ' => $data['phoneNumber'],
                'Email' => $data['email'],
                'Курс' => $event->getEventName()
            ];

            foreach($arr as $key => $value) {
                $txt .= "<b>".$key."</b> ".$value."%0A";
            };

            $sendToTelegram = fopen("https://api.telegram.org/bot{$token}/sendMessage?chat_id={$chat_id}&parse_mode=html&text={$txt}","r");

            foreach($mails as $mail) {
                $message = new \Swift_Message('Запрос программы бронирования');
                $message->setFrom('web.interactive.kg@gmail.com');

                $message->setTo($mail);

                $message->setBody(
                    $this->renderView(
                        'events_module/booking_messages/send.program.html.twig', [
                            'user' => $data['fullName'],
                            'number' => $data['phoneNumber'],
                            'email' => $data['email'],
                            'event' => $event->getEventName()
                        ]
                    ),
                    'text/html'
                );

                $mailer->send($message);
            }
            return $this->redirect($request->server->get('HTTP_REFERER'));
        }
        return $this->render('events_module/single_events.html.twig', [
            'form' => $form->createView(),
            'event' => $event,
            'eventSession' => $eventSessions[0]
        ]);
    }


    /**
     * @Route("/events/booking/{id}", requirements={"id": "\d+"}, name="app_events_booking")
     * @param EventModuleRepository $eventModuleRepository
     * @param EventBookingRepository $eventBookingRepository
     * @param ObjectManager $objectManager
     * @param int $id
     * @param \Swift_Mailer $mailer
     * @param SiteSettingsRepository $settingsRepository
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function bookUserAction(EventModuleRepository $eventModuleRepository,
                                   EventBookingRepository $eventBookingRepository,
                                   ObjectManager $objectManager,
                                   int $id,
                                   \Swift_Mailer $mailer,
                                    SiteSettingsRepository $settingsRepository)
    {
        if ($this->getUser()) {
            /**
             * @var $event EventModule
             */
            $event = $eventModuleRepository->getEventById($id);

            /**
             * @var $sessions EventSession
             */
            $sessions = $event->getEventSessions();
            $settings = $settingsRepository->getEmails();
            $mails = explode(',',$settings->getAdminEmail());
            foreach ($sessions as $session) {
                if ($eventBookingRepository->isAlreadyBooked($session, $this->getUser()) === null) {
                    $booking = new EventBooking();
                    $booking->setUser($this->getUser());
                    $event->setOrderLimit($event->getOrderLimit()-1);
                    $booking->setEvent($session);
                    $booking->setPayment(false);
                    $objectManager->persist($booking);

                    foreach($mails as $mail){
                        $message = new \Swift_Message('Новое бронирование');
                            $message->setFrom('web.interactive.kg@gmail.com');

                                 $message->setTo($mail);

                            $message->setBody(
                                $this->renderView(
                                // templates/emails/registration.html.twig
                                    'events_module/booking_messages/send.html.twig',[
                                        'user' => $this->getUser(),
                                        'booking' => $booking,
                                        'home_url' =>  'http://'.$_SERVER['HTTP_HOST'].'/admin',
                                        'send_event_url' => 'http://'.$_SERVER['HTTP_HOST'].'/events/'.$event->getId(),
                                    ]
                                ),
                                'text/html'
                            );

                         $mailer->send($message);
                    }
                } else {
                    $this->addFlash("error", "Вы уже записаны на {$event->getEventName()}");
                    return $this->redirectToRoute('app_my_events');
                }
            }
            $objectManager->flush();
            $this->addFlash("notice", "Спасибо, что записались на {$event->getEventName()}");
            return $this->redirectToRoute('app_my_events');
        } else {
            return $this->redirectToRoute('fos_user_security_login');
        }
    }

}