<?php


namespace App\Controller;


use App\Repository\SliderModuleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class SliderController extends Controller
{

    /**
     * @Route("slider", name="app_slider")
     * @param SliderModuleRepository $sliderModuleRepository
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showOnMainAction(SliderModuleRepository $sliderModuleRepository){

        $sliders = $sliderModuleRepository->getIsActiveSliders();


        return $this->render('slider/show_slider.html.twig',[
            'sliders' => $sliders
        ]);
    }

}