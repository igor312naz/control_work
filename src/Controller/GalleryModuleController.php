<?php

namespace App\Controller;

use App\Repository\GalleryImageRepository;
use App\Repository\GalleryModuleRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Model\Gallery\GalleryHandler;


class GalleryModuleController extends Controller
{
    /**
     * @Route("gallery", name="app_page_gallery")
     * @param GalleryModuleRepository $galleryModuleRepository
     * @param GalleryImageRepository $galleryImageRepository
     * @param GalleryHandler $galleryHandler
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAllGalleryAction(GalleryModuleRepository $galleryModuleRepository,
                                         GalleryImageRepository $galleryImageRepository, GalleryHandler $galleryHandler)
    {
        list($gallery, $images) = $galleryHandler->getGalleryData($galleryModuleRepository, $galleryImageRepository);

        return $this->render('gallery/page_gallery.html.twig', [
            'gallery' => $gallery,
            'images' => $images
        ]);
    }

    /**
     * @Route("/gallery/image/{id}", requirements={"id": "\d+"}, name="app_gallery_image")
     * @param $id
     * @param GalleryModuleRepository $galleryModuleRepository
     * @param GalleryHandler $galleryHandler
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showImagesFromGalleryAction($id, GalleryModuleRepository $galleryModuleRepository, GalleryHandler $galleryHandler)
    {
        list($images, $description, $albumName) = $galleryHandler->getDataForImages($id, $galleryModuleRepository);


        return $this->render('gallery/page_gallery_images.html.twig', [
            'images' => $images,
            'description' => $description,
            'album_name' => $albumName

        ]);
    }


}
