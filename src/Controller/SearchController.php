<?php


namespace App\Controller;

use App\Model\Search\SearchHandler;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class SearchController extends Controller
{
    /**
     * @Route("/search", name="app_search", requirements={"route"=".+"})
     * @param SearchHandler $searchHandler
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function searchAction(
        SearchHandler $searchHandler,
        Request $request)
    {
        $word = trim($request->query->get('word'));
        if (strlen($word) == 0) {
            return $this->render('search/search_error.html.twig', [
                'error' => "Не указано слово для поиска"
            ]);
        }

        $eventResults = $searchHandler->searchInEventModule($word);
        $newsResults = $searchHandler->searchInNewsModule($word);
        $dynamicPageResults = $searchHandler->searchInDynamicPageModule($word);

        if (empty($dynamicPageResults) &&
            empty($newsResults) &&
            empty($eventResults)) {
            return $this->render('search/search_error.html.twig', [
                'error' => "По запросу: «{$word}» ничего не найдено"
            ]);
        }

        return $this->render('search/search.html.twig', [
            'dynamicPages' => $dynamicPageResults,
            'news' => $newsResults,
            'events' => $eventResults
        ]);
    }


}