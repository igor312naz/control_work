<?php

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class NewsModuleAdmin extends AbstractAdmin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('title', null, array('label' => 'Название'))
            ->add('date', null, array('label' => 'Дата'))
            ->add('shortDescription', null, array('label' => 'Краткое описание'))
            ->add('fullDescription', null, array('label' => 'Полное описание'));
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('image', null, array('label' => 'Изображение', 'template' => 'SonataAdmin/list_mapper_twigs/list_image.html.twig'))
            ->add('title', null, array('label' => 'Название'))
            ->add('date', null, array('label' => 'Дата'))
            ->add('shortDescription', null, array('label' => 'Краткое описание'))
            ->add('fullDescription', null, array('label' => 'Полное описание'))
            ->add('_action', null, array('label' => 'Действие',
                'actions' => array(
                    'edit' => array(),
                    'delete' => array(),
                ),
            ));
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('title', TextType::class, array('label' => 'Название'))
            ->add('date', DateType::class, array('label' => 'Дата',
                'widget' => 'single_text'
            ))
            ->add('imageFile', FileType::class, array('label' => 'Изображение', 'required' => false))
            ->add('shortDescription', TextType::class, array('label' => 'Краткое описание'))
            ->add('fullDescription', TextareaType::class, array('label' => 'Полное описание', 'attr' => array('class' => 'ckeditor')));
    }
}