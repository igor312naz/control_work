<?php

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class SliderModuleAdmin extends AbstractAdmin
{

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('name',null,array('label' => 'Наименование'))
            ->add('text',null,array('label' => 'Текст'))
            ->add('link',null,array('label' => 'Ссылка'))
            ->add('isActive',null,array('label' => 'Доступность'))
            ->add('image',null,array('label' => 'Изображение'));
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {

        $listMapper
            ->add('id')
            ->add('image',null,array('label' => 'Изображение','template' => 'SonataAdmin/list_mapper_twigs/list_image.html.twig'))
            ->add('name',null,array('label' => 'Наименование'))
            ->add('text',null,array('label' => 'Текст'))
            ->add('link',null,array('label' => 'Ссылка'))
            ->add('isActive',null,array('label' => 'Доступность'))

            ->add('_action', null, array('label' => 'Действие',
                'actions' => array(
                    'edit' => array(),
                    'delete' => array(),
                ),
            ));
    }


    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name',TextType::class,array('label' => 'Наименование'))
            ->add('text',TextType::class,array('label' => 'Текст'))
            ->add('link',TextType::class,array('label' => 'Ссылка '))
            ->add('isActive',ChoiceType::class,array('label' => 'Доступность',
                'choices' => [
                    'Да' => true,
                    'Нет' => false
                ]
            ))
            ->add('imageFile',FileType::class,array('label' => 'Изображение','required' => false));

    }
}