<?php

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class CoachModuleAdmin extends AbstractAdmin
{

    public function createQuery($context = 'list')
    {
        $proxyQuery = parent::createQuery('list');
        // Default Alias is "o"
        // You can use id to hide root element
        //$proxyQuery->where('o.id != 1');
        $proxyQuery->addOrderBy('o.priority', 'ASC');
        return $proxyQuery;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('priorityUp', $this->getRouterIdParameter() . '/priorityUp');
        $collection->add('priorityDown', $this->getRouterIdParameter() . '/priorityDown');
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('fullName', null, array('label' => 'Фамилия Имя Отчество'))
            ->add('image', null, array('label' => 'Картинка'))
            ->add('shortDescription', null, array('label' => 'Краткое описание'))
            ->add('fullDescription', null, array('label' => 'Полное описание'))
            ->add('priority', null, array('label' => 'Приоритет'))
            ->add('isActive', null, array('label' => 'Доступность'));
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('fullName', null, array('label' => 'ФИО'))
            ->add('imageFile', null, array('label' => 'Изображение', 'template' => 'SonataAdmin/list_mapper_twigs/list_image.html.twig'))
            ->add('shortDescription', null, array('label' => 'Краткое описание'))
            ->add('fullDescription', null, array('label' => 'Полное описание'))
            ->add('isActive', null, array('label' => 'Доступность'))
            ->add('_action', null, array('label' => 'Действие',
                'actions' => array(
                    'edit' => array(),
                    'delete' => array(),
                    'priorityUp' => array(
                        'template' => 'SonataAdmin/CRUD/couch/up.html.twig'
                    ),
                    'priorityDown' => array(
                        'template' => 'SonataAdmin/CRUD/couch/down.html.twig'
                    )
                ),
            ));
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {

        $manager = $this->getConfigurationPool()
            ->getContainer()->get('doctrine')
            ->getManager()
            ->getRepository('App\Entity\CoachModule');
        $position = $manager->findLastElementPosition();
        $position = $position['priority'];
        if ($position == null){
            $position = 1;
        }else{
            $position++;
        }

        $formMapper
            ->add('fullName', null, array('label' => 'Фамилия Имя Отчество'))
            ->add('imageFile', FileType::class,  array('label' => 'Изображение', 'required' => false))
            ->add('shortDescription', null, array('label' => 'Краткое описание'))
            ->add('fullDescription', TextareaType::class, array('label' => 'Полное описание', 'attr' => array('class' => 'ckeditor')))
            ->add('priority', HiddenType::class, array('data' => $position))
            ->add('isActive', null, array('label' => 'Доступность'));

    }


    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('fullName')
            ->add('shortDescription')
            ->add('fullDescription')
            ->add('priority')
            ->add('isActive')
            ->add('imageFile', null, array('template' => 'SonataAdmin/list_mapper_twigs/list_image.html.twig'));
    }
}