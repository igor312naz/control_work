<?php

namespace App\Admin;



use App\Entity\ContactsModule;
use App\Types\GoogleMapType\ShippingType;
use Doctrine\Common\Collections\ArrayCollection;
use function PHPSTORM_META\map;
use function Sodium\add;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\CoreBundle\Form\Type\CollectionType;
use Sonata\CoreBundle\Form\Type\ImmutableArrayType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class ContactsModuleAdmin extends AbstractAdmin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('email',null,array('label => Почтовый ящик'))
            ->add('fax',null, array('label' => 'Факс'))
            ->add('phonesForViewSonata',null, array('label' => 'Телефоны'))
            ->add('address',null, array('label' => 'Адрес'))
            ->add('coordinate',null, array('label' => 'Координаты'));
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('email',null,array('label => Почтовый ящик'))
            ->add('fax',null, array('label' => 'Факс'))
            ->add('phonesForViewSonata',null, array('label' => 'Телефоны'))
            ->add('address',null, array('label' => 'Адрес'))
            ->add('coordinate',null,array('template' => 'SonataAdmin/CRUD/contacts_pages/contacts_admin_page.html.twig','label'=>'Координаты'))
            ->add('_action', null, array(
                'actions' => array(
                    'edit' => array(),
                    'delete' => array(),
                ),'label'=>'Действие',
            ));
    }


    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('email',EmailType::class,['label' => 'Почтовый ящик'])
            ->add('fax',TextType::class,['label' => 'Факс'])
            ->add('address',TextType::class,['label' => 'Адресс','attr' => ['class'=> 'address' ]])

            ->add('phones', 'collection', array(
                'label' => 'Телефон',
                'entry_type'    => TextType::class,
                'allow_add'     => true,
                'by_reference'  => false,
                'entry_options'  => array(
                   'attr' => [
                       'placeholder' => 'Новый номер'
                   ]
                ),

            ))
            ->add('coordinate',TextType::class,['label' => 'Координаты','attr' =>
                [
                    'class'=> 'coordinate',
                    'readonly' => true
                ]])
            ->add('map',ShippingType::class,['label' => 'Отметье точку на карте']);

    }
}