<?php

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Sonata\AdminBundle\Route\RouteCollection;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

class GalleryModuleAdmin extends AbstractAdmin
{
    public function createQuery($context = 'list')
    {
        $proxyQuery = parent::createQuery('list');
        // Default Alias is "o"
        // You can use id to hide root element
        //$proxyQuery->where('o.id != 1');
        $proxyQuery->addOrderBy('o.priority', 'ASC');
        return $proxyQuery;
    }


    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('upload', $this->getRouterIdParameter().'/upload');
        $collection->add('fileUploadHandler', $this->getRouterIdParameter() . '/fileUploadHandler');
        $collection->add('sort', $this->getRouterIdParameter().'/sort');
        $collection->add('sorting');
        $collection->add('remove', $this->getRouterIdParameter().'/remove');
        $collection->add('priorityUp', $this->getRouterIdParameter() . '/priorityUp');
        $collection->add('priorityDown', $this->getRouterIdParameter() . '/priorityDown');

    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('albumName', null, array('label' => 'Название'))
            ->add('shortDescription', null, array('label' => 'Краткое описание'))
            ->add('data')
            ->add('isActive');
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('albumName', null, array('label' => 'Название'))
            ->add('shortDescription', null, array('label' => 'Краткое описание'))
            ->add('data')
            ->add('isActive', null, array('label' => 'Доступность',  'editable' => true))
            ->add('_action', null, array(
                'label' => 'Действие',
                'actions' => array(
                    'edit'=> array(),
                    'delete' => array(),
                    'upload' => array(
                        'template' => 'SonataAdmin/CRUD/gallery/uploadAction.html.twig'
                    ),
                    'sort' => array(
                        'template' => 'SonataAdmin/CRUD/gallery/sortAction.html.twig'
                    ),
                    'priorityUp' => array(
                        'template' => 'SonataAdmin/CRUD/couch/up.html.twig'
                    ),
                    'priorityDown' => array(
                        'template' => 'SonataAdmin/CRUD/couch/down.html.twig'
                    )
                ),
            ));
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {

        $manager = $this->getConfigurationPool()
            ->getContainer()->get('doctrine')
            ->getManager()
            ->getRepository('App\Entity\GalleryModule');
        $position = $manager->findLastElementPosition();
        $position = $position['priority'];
        if ($position == null){
            $position = 1;
        }else{
            $position++;
        }

        $formMapper
            ->add('albumName', null, array('label' => 'Название'))
            ->add('shortDescription', null, array('label'=> 'Краткое описание'))
            ->add('data', DateType::class, array('label' => 'Дата', 'widget' => 'single_text'))
            ->add('priority', HiddenType::class, array('data' => $position))
            ->add('isActive', null, array('label' => 'Доступность'))
        ;
    }
}