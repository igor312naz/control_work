<?php

namespace App\Admin;

use App\Entity\CoachModule;
use App\Entity\EventModule;
use App\Entity\EventSession;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use App\Entity\EventCategory;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class EventFileAdmin extends AbstractAdmin
{

    protected $parentAssociationMapping = 'event';
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('name', null, array('label' => 'Название'))
            ->add('material', null, array('label' => 'Файл'))
            ->add('event', null, array('label' => 'Мероприятие'));
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('name', null, array('label' => 'Название'))
            //->add('event', EntityType::class, array('label' => 'Мероприятие', 'class' => EventSession::class))
            ->add('material', null, array('label' => 'Название файла'))
            ->add('fileMaterial', null, array('label' => 'Скачать файл', 'template' => 'SonataAdmin/CRUD/events/download_file.html.twig'))
            ->add('_action', null, array('label' => 'Действие',
                'actions' => array(
                    'edit' => array(),
                    'delete' => array(),
                ),
            ));
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('event.event.eventName', null, array(
                'label' => 'Мероприятие',
                'attr' => array(
                    'readonly' => true,
                )
            ))
            ->add('event.name', null, array(
                'label' => 'Сеанс',
                'attr' => array(
                    'readonly' => true,
                )
            ))
            ->add('name', null, array('label' => 'Название'))
            //->add('event', EntityType::class, array('label' => 'Мероприятие', 'class' => EventSession::class))
            ->add('fileMaterial', FileType::class, array('label' => 'Файл', 'required' => false));
    }
}