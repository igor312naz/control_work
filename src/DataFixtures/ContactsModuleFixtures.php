<?php
/**
 * Created by PhpStorm.
 * User: maks
 * Date: 27.07.18
 * Time: 12:16
 */

namespace App\DataFixtures;


use App\Entity\ContactsModule;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class ContactsModuleFixtures extends Fixture
{

    private  $emails = ['marketing@interactive.kg','marketing@interactive.kg',];
    private  $phones = [
        ['+996 (312) 69-92-79','+996 (556) 69-92-79'],
        ['+996 (312) 69-92-79','+996 (556) 69-92-79']
    ];
    private  $faxs = ['+996 (312) 69-92-79','+996 (312) 69-92-79',];
    private  $adress = [
        'г. Бишкек, пр. Чуй, 230 (пересекает ул. Фучика), (БЦ "Берекет", 9 этаж)',
        'г. Бишкек, пр. Некрасова, 47 (пересекает ул. Гагарина)'];
    private  $coordinate = [
        '42.85287446281948,74.55916088867184',
        '42.864451153620074,74.5790736083984'
    ];

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {

        for ($i = 0; $i < count($this->emails); $i++){
            $ContactsModule = new ContactsModule();
            $ContactsModule
                ->setEmail($this->emails[$i])
                ->setPhones($this->phones[$i])
                ->setAddress($this->adress[$i])
                ->setFax($this->faxs[$i])
                ->setCoordinate($this->coordinate[$i]);
            $manager->persist($ContactsModule);
        }
        $manager->flush();
    }
}