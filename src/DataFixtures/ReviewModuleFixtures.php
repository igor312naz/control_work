<?php

namespace App\DataFixtures;


use App\Entity\ReviewModule;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class ReviewModuleFixtures extends Fixture
{
    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {

        $names = ['Рассказ участника курса НЛП-Практик в центре "ИнтерАктив"',
            'Отзыв участницы о курсе НЛП',
            'отзыв о курсе "НЛП-Практик" от компании "ИнтерАктив"'];

        $links = ["<iframe width=\"350\" height=\"250\" src=\"https://www.youtube.com/embed/vCvuVWK-3Rk\" frameborder=\"0\" allow=\"autoplay; encrypted-media\" allowfullscreen></iframe>",
            "<iframe width=\"350\" height=\"250\" src=\"https://www.youtube.com/embed/-GVw3-P1HqU\" frameborder=\"0\" allow=\"autoplay; encrypted-media\" allowfullscreen></iframe>",
            "<iframe width=\"350\" height=\"250\" src=\"https://www.youtube.com/embed/lpJd1VPzgiM\" frameborder=\"0\" allow=\"autoplay; encrypted-media\" allowfullscreen></iframe>"];


        for ($i = 0; $i < 3; $i++) {
            $reviewModule = new ReviewModule();
            $reviewModule
                ->setName($names[$i])
                ->setLink($links[$i])
                ->setIsActive(1);

            $manager->persist($reviewModule);
        }
        $manager->flush();
    }

}