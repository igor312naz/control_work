<?php

namespace App\DataFixtures;


use App\Entity\EventSession;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class EventSessionFixtures extends Fixture implements DependentFixtureInterface
{
    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $event1 = $this->getReference('eventModule1');
        $event2 = $this->getReference('eventModule2');
        $event3 = $this->getReference('eventModule3');


        $eventSession1 = new EventSession();
        $eventSession1
            ->setName('Сеанс 1')
            ->setEvent($event1)
            ->setDate(new \DateTime('2019-10-07 12:00:00'))
            ->setPrice(1000);
        $manager->persist($eventSession1);
        $this->addReference('eventSession1', $eventSession1);


        $eventSession2 = new EventSession();
        $eventSession2
            ->setName('Сеанс 1')
            ->setEvent($event2)
            ->setDate(new \DateTime('2019-10-09 12:00:00'))
            ->setPrice(1000);
        $manager->persist($eventSession2);
        $this->addReference('eventSession2', $eventSession2);


        $eventSession3 = new EventSession();
        $eventSession3
            ->setName('Сеанс 1')
            ->setEvent($event3)
            ->setDate(new \DateTime('2018-08-07 12:00:00'))
            ->setPrice(1000);
        $manager->persist($eventSession3);
        $this->addReference('eventSession3', $eventSession3);


        $eventSession4 = new EventSession();
        $eventSession4
            ->setName('Сеанс 2')
            ->setEvent($event3)
            ->setDate(new \DateTime('2018-08-20 12:00:00'))
            ->setPrice(1000);
        $manager->persist($eventSession4);
        $this->addReference('eventSession4', $eventSession4);


        $manager->flush();
    }

    public function getDependencies()

    {

        return array(

            EventModuleFixtures::class

        );

    }

}