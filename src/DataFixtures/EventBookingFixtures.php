<?php

namespace App\DataFixtures;


use App\Entity\EventBooking;
use App\Entity\EventFile;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class EventBookingFixtures extends Fixture implements DependentFixtureInterface
{
    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $eventSession1 = $this->getReference('eventSession1');
        $eventSession2 = $this->getReference('eventSession2');
        $eventSession3 = $this->getReference('eventSession3');
        $eventSession4 = $this->getReference('eventSession4');

        $user = $this->getReference('user');
        $admin = $this->getReference('admin');


        $eventBooking1 = new EventBooking();
        $eventBooking1
            ->setDate(new \DateTime('2018-08-07 13:51:58'))
            ->setEvent($eventSession1)
            ->setUser($user)
            ->setPayment(0);
        $manager->persist($eventBooking1);




        $eventBooking2 = new EventBooking();
        $eventBooking2
            ->setDate(new \DateTime('2018-09-07 13:51:58'))
            ->setEvent($eventSession2)
            ->setUser($user)
            ->setPayment(1);
        $manager->persist($eventBooking2);





        $eventBooking3 = new EventBooking();
        $eventBooking3
            ->setDate(new \DateTime('2018-09-07 13:51:58'))
            ->setEvent($eventSession4)
            ->setUser($admin)
            ->setPayment(0);
        $manager->persist($eventBooking3);



        $eventBooking4 = new EventBooking();
        $eventBooking4
            ->setDate(new \DateTime('2018-08-06 13:51:58'))
            ->setEvent($eventSession4)
            ->setUser($user)
            ->setPayment(1);
        $manager->persist($eventBooking4);


        $eventBooking5 = new EventBooking();
        $eventBooking5
            ->setDate(new \DateTime('2018-08-06 13:51:58'))
            ->setEvent($eventSession3)
            ->setUser($admin)
            ->setPayment(0);
        $manager->persist($eventBooking5);
        $manager->flush();

        $eventBooking6 = new EventBooking();
        $eventBooking6
            ->setDate(new \DateTime('2018-08-06 13:51:58'))
            ->setEvent($eventSession3)
            ->setUser($user)
            ->setPayment(1);
        $manager->persist($eventBooking6);
        $manager->flush();

    }

    public function getDependencies()

    {

        return array(

            EventSessionFixtures::class,
            UserFixtures::class

        );

    }

}