<?php

namespace App\Form;



use App\Model\Enumeration\ClientTypeEnumeration;
use Sonata\AdminBundle\Form\Type\Filter\NumberType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class ProgramType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('phoneNumber',TelType::class,[
                'label' => 'Телефон',
                'attr'=>[
                    'pattern' => "0[0-9]{3}[0-9]{2}[0-9]{2}[0-9]{2}",
                    'placeholder'=>"Пример, 0772164598"
                ]
            ])
            ->add('fullName',TextType::class,[
                'label' => 'Ф.И.О'
            ])
            ->add('email', EmailType::class, [
                'label' => 'Почта'
            ])
            ->add('Получить', SubmitType::class);
    }

}
