<?php

namespace App\Form;



use App\Model\Enumeration\ClientTypeEnumeration;
use Symfony\Component\Form\AbstractType;;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

class AvatarType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('avatar',FileType::class,[
                'label' => 'Сменить аватар'
            ])
            ->add('Change',SubmitType::class);

    }


    public function getBlockPrefix()
    {
        return 'app_user_change_avatar';
    }
}
