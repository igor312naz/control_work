<?php
/**
 * Created by PhpStorm.
 * User: kseniya
 * Date: 27.08.18
 * Time: 19:23
 */

namespace App\Repository;

use App\Entity\SiteSettings;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bridge\Doctrine\RegistryInterface;


/**
 * @method SiteSettings|null find($id, $lockMode = null, $lockVersion = null)
 * @method SiteSettings|null findOneBy(array $criteria, array $orderBy = null)
 * @method SiteSettings[]    findAll()
 * @method SiteSettings[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SiteSettingsRepository extends ServiceEntityRepository
{
    public function __construct(
        RegistryInterface $registry
    )
    {
        parent::__construct($registry, SiteSettings::class);
    }

    public function getEmails(): ?SiteSettings
    {
        try {
            return $this->createQueryBuilder('s')
                ->select('s')
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

}