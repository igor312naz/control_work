<?php

namespace App\Repository;


use App\Entity\EventBooking;
use App\Entity\EventFile;
use App\Entity\EventSession;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method EventSession|null find($id, $lockMode = null, $lockVersion = null)
 * @method EventSession|null findOneBy(array $criteria, array $orderBy = null)
 * @method EventSession[]    findAll()
 * @method EventSession[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EventSessionRepository extends ServiceEntityRepository
{
    public function __construct(
        RegistryInterface $registry
    )
    {
        parent::__construct($registry, EventSession::class);
    }

    public function getSessionsByEvent($event)
    {
            return $this->createQueryBuilder('s')
                ->select('s')
                ->where('s.event = :event')
                ->setParameter('event', $event)
                ->getQuery()
                ->getResult();
    }
}
