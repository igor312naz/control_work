<?php

namespace App\Repository;



use App\Entity\SliderModule;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method SliderModule|null find($id, $lockMode = null, $lockVersion = null)
 * @method SliderModule|null findOneBy(array $criteria, array $orderBy = null)
 * @method SliderModule[]    findAll()
 * @method SliderModule[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SliderModuleRepository extends ServiceEntityRepository
{
    public function __construct(
        RegistryInterface $registry
    )
    {
        parent::__construct($registry, SliderModule::class);
    }


    public function getIsActiveSliders()
    {

            return $this->createQueryBuilder('s')
                ->select('s')
                ->where('s.isActive = :true')
                ->setParameter('true',1)
                ->getQuery()
                ->getResult();

    }


}