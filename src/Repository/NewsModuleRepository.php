<?php

namespace App\Repository;

use App\Entity\NewsModule;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method NewsModule|null find($id, $lockMode = null, $lockVersion = null)
 * @method NewsModule|null findOneBy(array $criteria, array $orderBy = null)
 * @method NewsModule[]    findAll()
 * @method NewsModule[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NewsModuleRepository extends ServiceEntityRepository
{


    public function __construct(
        RegistryInterface $registry
    )
    {
        parent::__construct($registry, NewsModule::class);
    }

    public function getHotNews()
    {


        return $this->createQueryBuilder('a')
            ->select('a')
            ->orderBy('a.date', 'DESC')
            ->getQuery()
            ->getResult();

    }

    public function searchByWord(?string $word)
    {
        return $this->createQueryBuilder('n')
            ->select('n.id, n.title, n.shortDescription')
            ->orWhere('n.title LIKE :word')
            ->orWhere('n.shortDescription LIKE :word')
            ->orWhere('n.fullDescription LIKE :word')
            ->setParameter('word', '%' . $word . '%')
            ->getQuery()
            ->getResult();
    }
}
