<?php

namespace App\Repository;

use App\Entity\CoachModule;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method CoachModule|null find($id, $lockMode = null, $lockVersion = null)
 * @method CoachModule|null findOneBy(array $criteria, array $orderBy = null)
 * @method CoachModule[]    findAll()
 * @method CoachModule[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CoachModuleRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, CoachModule::class);
    }

    /**
     * @param $priority
     * @return CoachModule[] Returns an array of CoachModule objects
     */

    public function findByPriorityField($priority)
    {
        try {
            return $this->createQueryBuilder('c')
                ->andWhere('c.priority = :priority')
                ->setParameter('priority', $priority)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    public function findAllAndSortByPriority()
    {

            return $this->createQueryBuilder('c')
                ->andWhere('c.isActive = 1')
                ->orderBy('c.priority')
                ->getQuery()
                ->getResult();

    }

    public function findLastElementPosition()
    {
        try {
            return $this->createQueryBuilder('c')
                ->select('c.priority')
                ->orderBy('c.priority', 'DESC')
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
        }
    }
}
