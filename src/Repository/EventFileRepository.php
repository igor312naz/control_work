<?php

namespace App\Repository;


use App\Entity\EventBooking;
use App\Entity\EventFile;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method EventFile|null find($id, $lockMode = null, $lockVersion = null)
 * @method EventFile|null findOneBy(array $criteria, array $orderBy = null)
 * @method EventFile[]    findAll()
 * @method EventFile[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EventFileRepository extends ServiceEntityRepository
{
    public function __construct(
        RegistryInterface $registry
    )
    {
        parent::__construct($registry, EventFile::class);
    }
    public function getFilesBySessionAndEvent($event,$user)
    {
        return $this->createQueryBuilder('f')
            ->select('f')
            ->innerJoin('f.event','s')
            ->innerJoin('s.eventBookings','b')
            ->where('b = :event')
            ->andWhere('s.event = :event')
            ->setParameter('event',$event)
            ->setParameter('user',$user)
            ->getQuery()
            ->getResult();
    }

}
