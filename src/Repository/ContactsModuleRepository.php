<?php

namespace App\Repository;

use App\Entity\ContactsModule;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ContactsModule|null find($id, $lockMode = null, $lockVersion = null)
 * @method ContactsModule|null findOneBy(array $criteria, array $orderBy = null)
 * @method ContactsModule[]    findAll()
 * @method ContactsModule[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ContactsModuleRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ContactsModule::class);
    }

}
