<?php

namespace App\Repository;


use App\Entity\DynamicPage;
use App\Entity\EventModule;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bridge\Doctrine\RegistryInterface;


/**
 * @method EventModule|null find($id, $lockMode = null, $lockVersion = null)
 * @method EventModule|null findOneBy(array $criteria, array $orderBy = null)
 * @method EventModule[]    findAll()
 * @method EventModule[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EventModuleRepository extends ServiceEntityRepository
{
    public function __construct(
        RegistryInterface $registry
    )
    {
        parent::__construct($registry, EventModule::class);
    }


    public function getActiveEvents()
    {
        try {
            return $this->createQueryBuilder('a')
                ->select('a')
                ->where('a.isActive = :isActive')
                ->setParameter('isActive', 1)
                ->orderBy('a.priority', 'asc')
                ->getQuery()
                ->getResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    public function getEventsByUser($user)
    {
        return $this->createQueryBuilder('e')
            ->select('e')
            ->where('b.user = :user')
            ->innerJoin('e.eventSessions', 's')
            ->innerJoin('s.eventBookings', 'b')
            ->orderBy('e.eventStart', 'asc')
            ->setParameter('user', $user)
            ->getQuery()
            ->getResult();

    }

    public function getEventsByCategory($category)
    {
        return $this->createQueryBuilder('e')
            ->select('e')
            ->innerJoin('e.categories', 'c')
            ->where('c.id = :category')
            ->setParameter('category', $category)
            ->getQuery()
            ->getResult();

    }




    public function getEventById($id)
    {
        try {
            return $this->createQueryBuilder('a')
                ->select('a')
                ->where('a.id = :id')
                ->setParameter('id', $id)
                ->andWhere('a.isActive = :isActive')
                ->setParameter('isActive', 1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    public function findByPriorityField($priority)
    {
        try {
            return $this->createQueryBuilder('c')
                ->andWhere('c.priority = :priority')
                ->setParameter('priority', $priority)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }


    public function findLastElementPosition()
    {
        try {
            return $this->createQueryBuilder('e')
                ->select('e.priority')
                ->orderBy('e.priority', 'DESC')
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
        }
    }


    public function searchByWord(?string $word)
    {
        return $this->createQueryBuilder('e')
            ->select('e.id, e.eventName as title, e.shortDescription')
            ->orWhere('e.eventName LIKE :word')
            ->orWhere('e.shortDescription LIKE :word')
            ->orWhere('e.fullDescription LIKE :word')
            ->andWhere('e.isActive = 1')
            ->setParameter('word', '%' . $word . '%')
            ->getQuery()
            ->getResult();
    }
}