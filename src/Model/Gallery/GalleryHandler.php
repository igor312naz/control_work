<?php

namespace App\Model\Gallery;


use App\Repository\GalleryImageRepository;
use App\Repository\GalleryModuleRepository;


class GalleryHandler
{
    /**
     * @param GalleryModuleRepository $galleryModuleRepository
     * @param GalleryImageRepository $galleryImageRepository
     * @return array
     */
    public function getGalleryData(GalleryModuleRepository $galleryModuleRepository, GalleryImageRepository $galleryImageRepository): array
    {
        $gallery = $galleryModuleRepository->findAllGalleryByDESC();
        $images = null;
        foreach ($gallery as $value) {
            $images [] = $galleryImageRepository->findByGalleryId($value->getId());
        }
        return array($gallery, $images);
    }

    /**
     * @param $id
     * @param GalleryModuleRepository $galleryModuleRepository
     * @return array
     */
    public function getDataForImages($id, GalleryModuleRepository $galleryModuleRepository): array
    {
        $images = $galleryModuleRepository->find($id)->getImages();
        $description = $galleryModuleRepository->find($id)->getShortDescription();
        $albumName = $galleryModuleRepository->find($id)->getAlbumName();
        return array($images, $description, $albumName);
    }
}