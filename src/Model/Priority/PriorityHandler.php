<?php

namespace App\Model\Priority;


use Doctrine\Common\Persistence\ObjectManager;

class PriorityHandler
{


    /**
     * @param $moduleRepository
     * @param ObjectManager $manager
     * @param $object
     * @param $priorityChange
     */
    public function changePriorityAction(
        $moduleRepository,
        ObjectManager $manager,
        $object,
        $priorityChange)
    {
        $priority = $object->getPriority() + $priorityChange;
        $objectToChange = $moduleRepository->findByPriorityField($priority);
        if ($objectToChange) {
            $objectToChange->setPriority($object->getPriority());
            $object->setPriority($priority);
            $manager->flush();
        }
    }
}