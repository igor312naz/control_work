<?php

namespace App\Model\Navigation;

use App\Entity\DynamicPage;
use App\Entity\EventCategory;
use App\Model\Enumeration\PageModulesEnumeration;
use App\Repository\DynamicPageRepository;
use App\Repository\EventCategoryRepository;
use Doctrine\ORM\PersistentCollection;
use Example\Entity\Category;
use Symfony\Component\DependencyInjection\ContainerInterface;

class NavigationHandler
{
    /**
     * @var ContainerInterface
     */
    private $container;
    /**
     * @var DynamicPageRepository
     */
    private $dynamicPageRepository;
    /**
     * @var EventCategoryRepository
     */
    private $eventCategoryRepository;

    public function __construct(
        DynamicPageRepository $dynamicPageRepository,
        EventCategoryRepository $eventCategoryRepository,
        ContainerInterface $container
    )
    {
        $this->container = $container;
        $this->dynamicPageRepository = $dynamicPageRepository;
        $this->eventCategoryRepository = $eventCategoryRepository;
    }

    public function getBreadCamps(string $fullPath)
    {
        $page = $this->getPageByPath($fullPath, true);

        if ($page == null) {
            return null;
        }

        $breadCamps = [];
        while ($page->getLevel() != 0) {
            $breadCamps[] = [
                "id" => $page->getRoute(),
                "route" => $page->getFullRoute(),
                "title" => $page->getTitle()];
            $page = $page->getParent();
        }
        return array_reverse($breadCamps);
    }

    /**
     * @param string $fullPath
     * @return array
     */
    public function getSidebarNavigation(string $fullPath)
    {
        $page = $this->getPageByPath($fullPath);

        if ($page == null) {
            return null;
        }

        if ($page->getPageModule() == PageModulesEnumeration::EVENTS) {
            return $this->getLinksForEventSidebar();
        }

        //Если у страницы нет дочерних разделов, то возвращаем страницы текущего раздела и названия активного раздела.
        //Если у страницы есть дочерние разделы, то возвращаем дочерние разделы.
        if ($page->getChildren()->isEmpty()) {
            return $this->getLinksForSidebar(
                $page->getParent()->getChildren(),
                $page->getParent()->getTitle(),
                $page->getTitle()
            );
        } else {
            return $this->getLinksForSidebar(
                $page->getChildren(),
                $page->getTitle()
            );
        }
    }


    /**
     * @param PersistentCollection $pages
     * @param string $navigationTitle
     * @param string $currentPageTitle
     * @return array
     */
    private function getLinksForSidebar(PersistentCollection $pages,
                                        string $navigationTitle,
                                        string $currentPageTitle = null)
    {
        $links = [];

        foreach ($pages as $page) {
            $active = false;
            if ($page->getTitle() == $currentPageTitle) {
                $active = true;
            }

            $links[] = [
                "id" => $page->getRoute(),
                "title" => $page->getTitle(),
                "route" => $page->getFullRoute(),
                "active" => $active,
                "route_name" => "app-dynamic-page",
                "param" => "route"
            ];
        }
        return array_merge(["title" => $navigationTitle], ["list" => $links]);
    }


    /**
     * @return array
     */
    public function getLinksForEventSidebar()
    {
        /**
         * @var $categories EventCategory[]
         */

        $links = $this->getEventCategoriesLinks();


        return array_merge(["title" => 'Категории'], ["list" => $links]);
    }


    /**
     * @param string $fullPath
     * @return array
     */
    private static function getPathArrayFromDirectory(string $fullPath): array
    {
        $pathArray = [];

        //обрезаем первый "/"
        $fullPath = substr($fullPath, 1);

        //Если роут окочанчивается на "/", то обрезаем роут, например: forCompany/companyDevelopment/teamBuilding/
        if (substr($fullPath, -1) === "/") {
            $fullPath = substr($fullPath, 0, -1);
        }

        //создаем массив из путей.
        if (strpos($fullPath, '/') !== false) {
            $pathArray = explode("/", $fullPath);
        }
        return $pathArray;
    }

    /**
     * @param string $fullPath
     * @param bool $forBreadcamps
     * @return DynamicPage|null
     */
    private function getPageByPath(string $fullPath, bool $forBreadcamps = false)
    {
        $pathArray = $this->getPathArrayFromDirectory($fullPath);

        // Если страница DynamicPage, то корневая директория в роуте является page,
        // Если страница модуля, то ищем страницу по названию модуля.
        if (reset($pathArray) == "page") {
            $page = $this->dynamicPageRepository->getPartialDynamicPageByRoute(end($pathArray));
            if ($forBreadcamps) {
                $page = $page->getParent();
            }
        } else {
            $page = $this->dynamicPageRepository->getDynamicPageByModuleName(reset($pathArray));
        }
        return $page;
    }

    public function getLinksForMainMenu()
    {
        $rootLinks = $this->dynamicPageRepository->getRootNavigationLinks();

        $categories_links = $this->getEventCategoriesLinks();

        foreach ($rootLinks as $key => $link) {
            $rootLinks[$key] = array_merge($rootLinks[$key], ["children" => $this->dynamicPageRepository->getChildLinksByParent($link['id'])]);
            if ($link["pageModule"] == PageModulesEnumeration::EVENTS) {
                $rootLinks[$key] = array_merge($rootLinks[$key], ["children" => $categories_links]);
            }
        }

        return $rootLinks;
    }

    /**
     * @return array
     */
    private function getEventCategoriesLinks(): array
    {
        $categories = $this->eventCategoryRepository->findAll();
        $links = [];
        foreach ($categories as $category) {
            $links[] = [
                "id" => $category->getId(),
                "title" => $category->getName(),
                "route" => $category->getId(),
                "active" => null,
                "route_name" => "app_events_category",
                "param" => "id"
            ];
        }
        return $links;
    }

}