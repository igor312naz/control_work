# language: ru

@sliderModule @fixtures
Функционал: Тестируем в админке SliderModule

  @adminPanel @loginAdmin
  Сценарий: Создание нового слайда в админке
    Допустим я перехожу в панель администратора
    И Я нажимаю на ссылку "Модули"
    И Я нажимаю на ссылку "Слайды"
    И Я нажимаю на ссылку "Добавить новый"
    И я заполняю поля данными
      | Наименование | Мастер класс по ремонту корпусной мебели                                                                                                                                                       |
      | Текст        | Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley |
      | Ссылка       | http://namba.kg                                                                                                                                                                                |
    И я добавляю в дирректорию"Изображение" файл "behat_slide.jpg"
    И я нажимаю на кнопку "btn_create_and_edit"
    Тогда я вижу слово "Элемент создан успешно" на странице

  @adminPanel @loginAdmin
  Сценарий: Редактирование слайда в админке
    Допустим я перехожу в панель администратора
    И Я нажимаю на ссылку "Модули"
    И Я нажимаю на ссылку "Слайды"
    И Я нажимаю на ссылку "Редактировать"
    Тогда я заполняю поля данными
      | Наименование | Мастер по всяким делам                                                                                                                                                                         |
      | Текст        | Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley |
      | Ссылка       | http://ts.kg                                                                                                                                                                                   |
    И я добавляю в дирректорию"Изображение" файл "behat_slide_2.jpg"
    И я нажимаю на кнопку "btn_update_and_edit"
    Тогда я вижу слово "Элемент успешно обновлен" на странице

  @adminPanel @loginAdmin
  Сценарий: Удаление слайда в админке
    Допустим я перехожу в панель администратора
    И Я нажимаю на ссылку "Модули"
    И Я нажимаю на ссылку "Слайды"
    И Я нажимаю на ссылку "Удалить"
    И я нажимаю на кнопку "Да, удалить"
    Тогда я вижу слово "Элемент успешно удален." на странице